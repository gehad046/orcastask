//
//  AppConstants.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/27/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//


import Foundation

enum URLS {
    //TODO:- make in-accissible properties private
  private  static let baseUrl = "http://api.football-data.org"
  private  static let versionNumber = "/v1"
  private  static let competiotions = "/competitions"
  private  static let teams = "/teams"
    case league
    case team(Int)
    case teamInfo(Int)
    var url: String {
        switch self {
        case .league: return URLS.baseUrl + URLS.versionNumber + URLS.competiotions
        case .team(let id): return URLS.baseUrl + URLS.versionNumber + URLS.competiotions + "/\(id)" + URLS.teams
        case .teamInfo(let id) : return URLS.baseUrl + URLS.versionNumber + URLS.teams + "/\(id)"
            
        }
    }
}

