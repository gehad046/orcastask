//
//  ServerManeger.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/27/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//


import Foundation
import Alamofire


enum ErrorCode:Int {
    case Caneled = -999
    case NoInternet = -1009
    case UnKnown = 000
}

typealias  errorType = (ErrorCode, Any?) -> ()

class NetworkManager {
    
    func contectToApiWith(url : String , methodType : HTTPMethod ,params : [String : Any]? , success: @escaping (Any) -> (), errorHandler: @ escaping errorType ){
        Alamofire.request(url,
                          method: methodType,
                          parameters: params,
                          encoding: URLEncoding.default).validate().responseJSON{ response in
                            if response.result.error != nil {
                                if let errorCodeValue = response.error?._code,
                                    let errorCode = ErrorCode(rawValue: errorCodeValue){
                                    errorHandler(errorCode, response.error)
                                } else {
                                    errorHandler(ErrorCode.UnKnown, response.error)
                                }
                                return
                            }
                            
                            if response.data?.count == 0 {
                                errorHandler(ErrorCode.UnKnown, "No Data Retrived")
                                return
                            }
                            
                            if let responseValue = response.value {
                                success(responseValue)
                            }
        }
        
    }    
}

