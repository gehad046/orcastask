//
//  Extension.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/28/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlert(title:String, message:String, closure:(()->())?) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            closure?()
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
}
}
