//
//  Squad.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/28/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//


import RealmSwift
class  Player : Object,Codable {
    
    @objc dynamic   var name : String?
    @objc dynamic var nationality : String?
    @objc dynamic  var position : String?
    
    override static func primaryKey() -> String? {
        return "name"
    }
}
