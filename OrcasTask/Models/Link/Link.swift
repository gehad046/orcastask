//
//  Link.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/27/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//


import RealmSwift

class Link :Object, Codable {
    
    @objc dynamic var players : Fixture?
    
    enum CodingKeys : String,CodingKey{
        
        case players
        
        
    }
}
