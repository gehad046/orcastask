//
//  Team.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/27/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//


import RealmSwift

class Team: Object,Codable {
    
    @objc dynamic   var link : Link?
    @objc dynamic   var crestUrl : String?
    @objc dynamic   var name : String?
    @objc dynamic   var shortName : String?
    
    override static func primaryKey() -> String? {
        return "name"
    }
    enum CodingKeys : String,CodingKey{
        
        case link = "_links"
        case crestUrl
        case name
        case shortName
        
        
    }
    
}
