//
//  TeamRoot.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/27/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//


class TeamRoot : Codable {
    var link : Link?
    var count : Int?
    var teams : [Team]?
    
    enum CodingKeys : String,CodingKey{
        
        case link = "_links"
        case count
        case teams
    }
    
}

