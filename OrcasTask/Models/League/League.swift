//
//  Root.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/27/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//


import RealmSwift

class League: Object,Codable{
    
    var link : Link?
    @objc dynamic  var caption : String?
    @objc dynamic  var id : Int = 0
    @objc dynamic  var league : String?
    @objc dynamic  var numberOfGames : Int = 0
    @objc dynamic  var numberOfTeams : Int = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    enum CodingKeys : String,CodingKey{
        
        case link = "_links"
        case caption
        case id
        case league
        case numberOfGames
        case numberOfTeams
       
        
    }
}


