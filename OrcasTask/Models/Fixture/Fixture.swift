//
//  Fixture.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/27/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//


import RealmSwift

class Fixture :Object, Codable {
    
    @objc dynamic   var href : String?
    
    override static func primaryKey() -> String? {
        return "href"
    }
}
