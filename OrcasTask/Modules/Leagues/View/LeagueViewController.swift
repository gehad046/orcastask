//
//  ViewController.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/27/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import RealmSwift

struct LeagueViewModel {
    var leagueName : String?
    var leagueCaption: String?
    var numberOfGames = 0
    var numberOfTeams = 0
    var id : Int?
}

class LeagueViewController: UIViewController ,NVActivityIndicatorViewable{
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    var leaguesViewModelArray = [LeagueViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        self.startAnimating()
        getLeagues()
    }
    
    func setUpTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "LeagueTableViewCell", bundle: nil), forCellReuseIdentifier: "LeagueTableViewCell")
    }
    
    func getLeagues(){
        
        LeaguePresenter.shared.loadLeagues(onSuccess: { [weak self](leaguesViewModel) in
            if let leagues = leaguesViewModel{
                self?.leaguesViewModelArray = leagues
                self?.tableView.reloadData()
                self?.stopAnimating()
            }
            }, onFaluire: { [weak self](error) in
                self?.stopAnimating()
                self?.showAlert(title: "", message: "something went wrong", closure: nil)
        })
    }
    
}
extension LeagueViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leaguesViewModelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCell", for: indexPath) as! LeagueTableViewCell
        let viewmodel =  self.leaguesViewModelArray[indexPath.row]
        cell.configure(league: viewmodel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: "TeamID") as? TeamViewController {
            controller.leagueID = leaguesViewModelArray[indexPath.row].id
            show(controller, sender: self)
        }
    }
}

