//
//  LeagueTableViewCell.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/27/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import UIKit

class LeagueTableViewCell: UITableViewCell {
    
    @IBOutlet weak fileprivate var leagueNameLabel: UILabel!
    @IBOutlet weak fileprivate var leagueShortNameLabel: UILabel!
    @IBOutlet weak fileprivate var numberOfTeamsLabel: UILabel!
    @IBOutlet weak fileprivate var numberOfGamesLabel: UILabel!
    
    func configure(league :LeagueViewModel){
        leagueNameLabel.text = league.leagueCaption
        leagueShortNameLabel.text = league.leagueName
        numberOfGamesLabel.text = "\(league.numberOfGames)  game"
        numberOfTeamsLabel.text = "\(league.numberOfTeams)  team"
        
        
    }
    
}
