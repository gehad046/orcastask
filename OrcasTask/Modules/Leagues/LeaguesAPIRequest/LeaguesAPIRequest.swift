//
//  LeaguesAPIRequest.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/31/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import Foundation

class LeaguesAPIRequest: NSObject {
    typealias leagueResultType = (([League]?)->())
    typealias errorCompletionType = ((ErrorCode?)->())
    
    private let networkManager = NetworkManager()
    private let decoder = JSONDecoder()
    
    func getLeagues(didDataReady : @escaping leagueResultType , andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        networkManager.contectToApiWith(url: URLS.league.url, methodType: .get, params: [:], success: { (json) in
            if let theJSONData = try? JSONSerialization.data(withJSONObject: json){
            
                let result = try? self.decoder.decode([League].self, from: theJSONData)
                guard let leagues  = result    else {return}
                didDataReady(leagues)
            }
        }) { (error, msg) in
            print (msg!)
            errorCompletion(error)
        }
    }
}
