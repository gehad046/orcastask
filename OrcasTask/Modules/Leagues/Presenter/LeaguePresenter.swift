//
//  LeaguePresenter.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/30/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class LeaguePresenter: NSObject {
    
    let realm = try! Realm()
    var leaguesArray = [League]()
    static var shared = LeaguePresenter()
    
    func loadLeagues (onSuccess : @escaping ([LeagueViewModel]?)->(), onFaluire : @escaping (ErrorCode) ->()){
        let realmLeaguesArray = realm.objects(League.self)
        if !realmLeaguesArray.isEmpty{
            
            getLeaguesFromCache(leaguesRealm: realmLeaguesArray)
            let leaguesViewModel = self.convertToLeagueViewModel(leagues: self.leaguesArray)
            onSuccess(leaguesViewModel)
        }
        
        
        let leaguesAPIRequest = LeaguesAPIRequest()
        leaguesAPIRequest.getLeagues(didDataReady: { [weak self](leagues) in
            guard self != nil else { return  }
            guard let leagues = leagues else {
                onFaluire(ErrorCode.UnKnown)
                return
            }
            
            self?.cashToRealm(leagues: leagues)
            
            let leaguesViewModels = self?.convertToLeagueViewModel(leagues: leagues)
            onSuccess(leaguesViewModels)
            
        }) { (error) in
            if  let error = error{
                onFaluire(error)
            }
        }
    }
    
    private func getLeaguesFromCache (leaguesRealm : Results<League>){
        self.leaguesArray.removeAll()
        for league in leaguesRealm{
            let leagueItem = League()
            leagueItem.id = league.id
            leagueItem.league = league.league
            leagueItem.caption = league.caption
            leagueItem.numberOfGames = league.numberOfGames
            leagueItem.numberOfTeams = league.numberOfTeams
            self.leaguesArray.append(league)
            print(self.realm.configuration.fileURL ?? "")
        }
    }
    
    private func cashToRealm(leagues:[League]){
        do {
            try self.realm.write {
                self.realm.add(leagues, update: true)
            }
        } catch {
            print("realm error")
        }
        
    }
    private func convertToLeagueViewModel (leagues: [League]) -> [LeagueViewModel]{
        var leagueViewModelArray = [LeagueViewModel]()
        for league in leagues {
            var leagueViewModel = LeagueViewModel()
            leagueViewModel.leagueName = league.league
            leagueViewModel.numberOfGames = league.numberOfGames
            leagueViewModel.numberOfTeams = league.numberOfTeams
            leagueViewModel.leagueCaption = league.caption
            leagueViewModel.id = league.id
            leagueViewModelArray.append(leagueViewModel)
        }
        return leagueViewModelArray
    }
    
    
}

