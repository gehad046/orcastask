//
//  TeamHeaderTableViewCell.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/28/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import UIKit

class TeamHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak fileprivate var teamNameLabel: UILabel!
    @IBOutlet weak fileprivate var teamShortNameLabel: UILabel!
    @IBOutlet weak var teamImage: UIImageView!
    
    func configure(team : TeamViewModel)
    {
        teamNameLabel.text = team.name
        teamShortNameLabel.text = team.shortName
        if let teamImageURL = team.crestUrl {
            teamImage.sd_setImage(with: URL(string: teamImageURL), placeholderImage:#imageLiteral(resourceName: "like"))
        } else {
            teamImage.image = #imageLiteral(resourceName: "like")
        }
    }
    
}
