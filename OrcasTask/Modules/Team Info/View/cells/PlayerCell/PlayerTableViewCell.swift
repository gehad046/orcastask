//
//  PlayerTableViewCell.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/29/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import UIKit

class PlayerTableViewCell: UITableViewCell {
    
    @IBOutlet weak fileprivate var playerNameLabel: UILabel!
    @IBOutlet weak fileprivate var playerPositionLabel: UILabel!
    @IBOutlet weak fileprivate var playerNationalityLabel: UILabel!
    
    func configure(player : PlayerViewModel){
        playerNameLabel.text = player.name
        playerNationalityLabel.text = player.nationality
        playerPositionLabel.text = player.position
    }
    
}
