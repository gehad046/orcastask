//
//  TeamInfoViewController.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/28/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class TeamInfoViewController: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedTeam : TeamViewModel?
    var playersURL = ""
    var playersViewoModelArray = [PlayerViewModel]()
    weak var playerPresenter : TeamInfoPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Players"
        setUpTableView()
        playerPresenter = TeamInfoPresenter()
        self.startAnimating()
        getPlayers()
    }
    func setUpTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "TeamHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "TeamHeaderTableViewCell")
        tableView.register(UINib(nibName: "PlayerTableViewCell", bundle: nil), forCellReuseIdentifier: "PlayerTableViewCell")
    }
    
    func getPlayers(){
        TeamInfoPresenter.shared.loadPlayers(playersUrl: playersURL, onSuccess: { [weak self](playersViewModel) in
            if let player = playersViewModel {
                self?.playersViewoModelArray = player
                self?.tableView.reloadData()
                self?.stopAnimating()
            }
        }) { [weak self ](error) in
            self?.stopAnimating()
            self?.showAlert(title: "", message: "something went wrong", closure: nil)
        }
    }
    
}
extension TeamInfoViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playersViewoModelArray.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamHeaderTableViewCell", for: indexPath) as! TeamHeaderTableViewCell
            cell.configure(team: selectedTeam!)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerTableViewCell", for: indexPath) as! PlayerTableViewCell
            cell.configure(player: playersViewoModelArray[indexPath.row - 1 ])
            return cell
        }
        
    }
}

