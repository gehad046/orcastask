//
//  TeamInfoApiRequest.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/31/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import Foundation

class TeamInfoApiRequest {
    let networkManager = NetworkManager()
    private let decoder = JSONDecoder()
    typealias errorCompletionType = ((ErrorCode?)->())
    typealias  teamInfoResultType = (([Player]?)->())
    
    func getPlayers(url : String ,didDataReady : @ escaping ([Player]?)->(),andErrorCompletion errorCompletion: @escaping errorCompletionType)
    {
        networkManager.contectToApiWith(url: url, methodType: .get, params: [:], success: { (json) in
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: json){
                
                let result = try? self.decoder.decode(PlayerRoot.self, from: theJSONData)
                guard let players  = result?.players   else {return}
                didDataReady(players)
            }
        }) { (error, msg) in
            print (error)
            errorCompletion(error)
        }
    }
    
    
}
