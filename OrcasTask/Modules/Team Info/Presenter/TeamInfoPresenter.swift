//
//  TeamInfoPresenter.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/31/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import Foundation
import RealmSwift

class TeamInfoPresenter{
   
    static let shared = TeamInfoPresenter()
    let realm = try! Realm()
    var playersArray = [Player]()
   
    
    func loadPlayers (playersUrl:String,onSuccess : @ escaping ([PlayerViewModel]?)->(), onFaluire : @ escaping (ErrorCode) ->()){
        let realmPlayersArray = realm.objects(Player.self)
        if !realmPlayersArray.isEmpty{
             getPlayesFromCache(playersRealm: realmPlayersArray)
            let playersViewModel = self.convertToPlayerViewModel(players: playersArray)
            onSuccess(playersViewModel)
        }
        let teamInfoApiRequest = TeamInfoApiRequest()
            teamInfoApiRequest.getPlayers(url: playersUrl, didDataReady: { [weak self](players) in
                guard self != nil else { return  }
                guard let players = players else {
                    //TODO:- call onfailure
                    onFaluire(ErrorCode.UnKnown)
                    return
                }
                
                self?.cashToRealm(players: players)
                let playersViewModels = self?.convertToPlayerViewModel(players: players)
                onSuccess(playersViewModels)
                
            }) { (error) in
                
                if let error = error{
                    onFaluire(error)
                }
            }

        
    }
    
    private func getPlayesFromCache (playersRealm : Results<Player>){
        
        self.playersArray.removeAll()
        for player in playersRealm{
            let playerItem = Player ()
            playerItem.name = player.name
            playerItem.position = player.position
            playerItem.nationality = player.nationality
            
            self.playersArray.append(playerItem)
            print(self.realm.configuration.fileURL ?? "")
        }
        
    }
    private func cashToRealm(players:[Player]){
        do {
            try self.realm.write {
                self.realm.add(players, update: true)
            }
        } catch {
            print("realm error")
        }
        
    }
    
    private func convertToPlayerViewModel (players: [Player]) -> [PlayerViewModel]{
        var playersViewModelArray = [PlayerViewModel]()
        for player in players {
            var playerViewModel = PlayerViewModel()
            playerViewModel.name = player.name
            playerViewModel.nationality = player.nationality
            playerViewModel.position = player.position
            
         
            playersViewModelArray.append(playerViewModel)
        }
        return playersViewModelArray
    }
    
}




struct PlayerViewModel {
    
     var name : String?
     var nationality : String?
     var position : String?
    
}
