//
//  TeamViewController.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/27/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class TeamViewController: UIViewController ,NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView: UITableView!
    
    var leagueID : Int?
    var teamViewModelArray = [TeamViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableView()
        self.startAnimating()
        getTeams()
        self.navigationItem.title = "Team"
    }
    
    func setTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "TeamTableViewCell", bundle: nil), forCellReuseIdentifier: "TeamTableViewCell")
        
    }
    
    func getTeams(){
        TeamPresenter.shared.loadTeams(leagueId: leagueID!, onSuccess: { [weak self](teamsViewModel) in
            if let teams = teamsViewModel{
                self?.teamViewModelArray = teams
                self?.tableView.reloadData()
                self?.stopAnimating()
            }
        }) { [weak self](error) in
            self?.stopAnimating()
            self?.showAlert(title: "", message: "something went wrong", closure: nil)
        }
    }
}


extension TeamViewController :UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teamViewModelArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamTableViewCell", for: indexPath) as! TeamTableViewCell
        cell.configer(team: teamViewModelArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "TeamInfoID") as! TeamInfoViewController
        //  getPlayers(url: (teamViewModelArray[indexPath.row].link?.players?.href)!)
        controller.navigationItem.title = "Team Details"
        controller.selectedTeam = teamViewModelArray[indexPath.row]
        guard let playersUrl = (teamViewModelArray[indexPath.row].link?.players?.href) else {return}
        controller.playersURL = playersUrl
        show(controller, sender: self)
    }
}
