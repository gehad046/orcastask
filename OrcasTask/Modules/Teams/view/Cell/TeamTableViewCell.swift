//
//  TeamTableViewCell.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/27/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import UIKit
import SDWebImage

class TeamTableViewCell: UITableViewCell {
    
    @IBOutlet weak fileprivate var  teamImage: UIImageView!
    @IBOutlet weak fileprivate var teamNameLabel: UILabel!
    @IBOutlet weak fileprivate var teamShortNameLabel: UILabel!
    
    func configer(team : TeamViewModel){
        teamNameLabel.text = team.name
        teamShortNameLabel.text = team.shortName
        if let teamImageURL = team.crestUrl{
            teamImage.sd_setImage(with: URL(string: teamImageURL), placeholderImage: #imageLiteral(resourceName: "like"))

        }
        else {
            teamImage.image = #imageLiteral(resourceName: "like")
        }
    }
    
}
