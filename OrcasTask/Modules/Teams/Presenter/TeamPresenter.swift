//
//  TeamPresenter.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/31/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import Foundation
import RealmSwift

class TeamPresenter {
    
    static let shared = TeamPresenter()
    let realm = try! Realm()
    var teamsArray = [Team]()
    
    func loadTeams (leagueId: Int,onSuccess : @ escaping ([TeamViewModel]?)->(), onFaluire : @ escaping (ErrorCode) ->()){
        let realmTeamsArray = realm.objects(Team.self)
        if !realmTeamsArray.isEmpty {
            
            getTeamsFromCache(teamsRealm: realmTeamsArray)
            let teamsViewModel = self.convertToLeagueViewModel(teams: self.teamsArray)
            onSuccess(teamsViewModel)
        }
        let teamAPIRequest = TeamsAPIRequest()
        teamAPIRequest.getTeams(leagueId: leagueId, didDataReady: { [weak self](teams) in
            guard self != nil else { return  }
            guard let teams = teams else {
                onFaluire(ErrorCode.UnKnown)
                
                return
            }
            
            self?.cashToRealm(teams: teams)
            let teamsViewModels = self?.convertToLeagueViewModel(teams: teams)
            onSuccess(teamsViewModels)
            
        }) { (error) in
            if let error = error{
                onFaluire(error)
            }
        }
        
    }
    
    private func getTeamsFromCache (teamsRealm : Results<Team>){
        self.teamsArray.removeAll()
        for team in teamsRealm{
            let teamItem = Team()
            teamItem.name = team.name
            teamItem.shortName = team.shortName
            teamItem.crestUrl = team.crestUrl
            teamItem.link = team.link
            self.teamsArray.append(team)
            print(self.realm.configuration.fileURL ?? "")
        }
    }
    
    private func cashToRealm(teams:[Team]){
        do {
            try self.realm.write {
                self.realm.add(teams, update: true)
            }
        } catch {
            print("realm error")
        }
        
    }
    private func convertToLeagueViewModel (teams: [Team]) -> [TeamViewModel]{
        var teamsViewModelArray = [TeamViewModel]()
        for team in teams {
            var teamViewModel = TeamViewModel()
            teamViewModel.name = team.name
            teamViewModel.crestUrl = team.crestUrl
            teamViewModel.shortName = team.shortName
            teamViewModel.link = team.link
            teamsViewModelArray.append(teamViewModel)
        }
        return teamsViewModelArray
    }
}

struct TeamViewModel {
    
    var link : Link?
    var crestUrl: String?
    var name : String?
    var shortName : String?
    
}
