//
//  TeamApiRequest.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/31/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//

import Foundation

class TeamsAPIRequest{
    
    private let networkManager = NetworkManager()
    private let decoder = JSONDecoder()
    typealias  teamResultType = (([Team]?)->())
    typealias errorCompletionType = ((ErrorCode?)->())
    
    func getTeams(leagueId: Int, didDataReady : @ escaping teamResultType ,andErrorCompletion errorCompletion: @escaping errorCompletionType){
        networkManager.contectToApiWith(url: URLS.team(leagueId).url, methodType: .get, params: [:], success: { (json) in
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: json){
                
                let result = try? self.decoder.decode(TeamRoot.self, from: theJSONData)
                guard let teams  = result?.teams    else {return}
                didDataReady(teams)
            }
        }) { (error, msg) in
            print (error)
            errorCompletion(error)
        }
    }
    
}
